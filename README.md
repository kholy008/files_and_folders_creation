
## Folder creator
This is a folder creator that require us to generate numerous number of folder. 

## Purpose of this project
Sometimes, we need to create numerous amount of folder into our directory during our studies, work and so on. Thus, this can help us to reduce our burden on doing a repetition task. 

## To-do before running the file 
For full functionality, please install the "requirements" which the text file named using pip 
`cd` to the directory you want the folders to presented at
---------------------
Run setup.py for it.
For **Linux** users, please run this command: "chmod +x setup.py" before execute setup.py

## Example output
```
KHOLY008@MLP112-11 MINGW64 ~/Desktop/files_and_folders_creation (master)
$ python folder_and_file_creation.py                                                                                                                                                                            
Enter a path: ..
Enter the name of the folder: UniSA_SP5
--------------------------------------------
Number of courses: 4
Name of the courses: Network Fundamentals
Name of the courses: Problem Solving and Programmming
Name of the courses: Data Driven Web Technologies
Name of the courses: Security Foundations
[+] Directory created
[+] Folder created!!
[*] week's folder created
    [+] Folder in week_1 created successfull
    [+] Folder in week_10 created successfull
    [+] Folder in week_11 created successfull
    [+] Folder in week_12 created successfull
    [+] Folder in week_2 created successfull
    [+] Folder in week_3 created successfull
    [+] Folder in week_4 created successfull
    [+] Folder in week_5 created successfull
    [+] Folder in week_6 created successfull
    [+] Folder in week_7 created successfull
    [+] Folder in week_8 created successfull
    [+] Folder in week_9 created successfull
[+] Folder created!!
[*] week's folder created
    [+] Folder in week_1 created successfull
    [+] Folder in week_10 created successfull
    [+] Folder in week_11 created successfull
    [+] Folder in week_12 created successfull
    [+] Folder in week_2 created successfull
    [+] Folder in week_3 created successfull
    [+] Folder in week_4 created successfull
    [+] Folder in week_5 created successfull
    [+] Folder in week_6 created successfull
    [+] Folder in week_7 created successfull
    [+] Folder in week_8 created successfull
    [+] Folder in week_9 created successfull
[+] Folder created!!
[*] week's folder created
    [+] Folder in week_1 created successfull
    [+] Folder in week_10 created successfull
    [+] Folder in week_11 created successfull
    [+] Folder in week_12 created successfull
    [+] Folder in week_2 created successfull
    [+] Folder in week_3 created successfull
    [+] Folder in week_4 created successfull
    [+] Folder in week_5 created successfull
    [+] Folder in week_6 created successfull
    [+] Folder in week_7 created successfull
    [+] Folder in week_8 created successfull
    [+] Folder in week_9 created successfull
[+] Folder created!!
[*] week's folder created
    [+] Folder in week_1 created successfull
    [+] Folder in week_10 created successfull
    [+] Folder in week_11 created successfull
    [+] Folder in week_12 created successfull
    [+] Folder in week_2 created successfull
    [+] Folder in week_3 created successfull
    [+] Folder in week_4 created successfull
    [+] Folder in week_5 created successfull
    [+] Folder in week_6 created successfull
    [+] Folder in week_7 created successfull
    [+] Folder in week_8 created successfull
    [+] Folder in week_9 created successfull
|-- Data_Driven_Web_Technologies
|   |-- week_1
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_10
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_11
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_12
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_2
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_3
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_4
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_5
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_6
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_7
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_8
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   `-- week_9
|       |-- Lecture
|       |-- Practical
|       `-- Workshop
|-- Network_Fundamentals
|   |-- week_1
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_10
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_11
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_12
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_2
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_3
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_4
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_5
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_6
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_7
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_8
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   `-- week_9
|       |-- Lecture
|       |-- Practical
|       `-- Workshop
|-- Problem_Solving_and_Programmming
|   |-- week_1
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_10
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_11
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_12
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_2
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_3
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_4
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_5
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_6
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_7
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   |-- week_8
|   |   |-- Lecture
|   |   |-- Practical
|   |   `-- Workshop
|   `-- week_9
|       |-- Lecture
|       |-- Practical
|       `-- Workshop
`-- Security_Foundations
    |-- week_1
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_10
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_11
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_12
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_2
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_3
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_4
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_5
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_6
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_7
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    |-- week_8
    |   |-- Lecture
    |   |-- Practical
    |   `-- Workshop
    `-- week_9
        |-- Lecture
        |-- Practical
        `-- Workshop
```