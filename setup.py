import subprocess
import platform

def main():
    if platform.system() == "Linux":
        subprocess.call("chmod +x *",shell=True)


    command_list = ["pip install -r requirements.txt",
                    "python -m pip install -r requirements.txt",
                    "py -m pip install -r requirements.txt",
                    "python3 -m pip install -r requirements.txt",
                    "pip install -U -r requirements.txt --user",
                    "python -m pip install -U -r requirements.txt --user",
                    "py -m pip install -U -r requirements.txt --user",
                    "python3 -m pip install -U -r requirements.txt --user"
                    ] # commands to run to install requirements.txt

    for command in command_list:
        try:
            subprocess.check_output(command, shell=True, stderr=subprocess.DEVNULL) # hide the downloading operation and error messages
        except subprocess.CalledProcessError as e:
            print(f"[!] Failed to execute this command => {command}")
        else:
            print(f"[*] Command: '{command}' => SUCCESS !!!")
            break

if __name__ == '__main__':
    main()
